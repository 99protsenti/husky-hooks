// We need this script as we want explicit CWD in case of repo root installation
if (!process.env.CI) require("husky").install(process.cwd())
