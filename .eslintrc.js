module.exports = {
  parserOptions: {
    ecmaVersion: "latest",
  },
  env: {
    node: true,
    commonjs: true,
    es6: true,
  },
  extends: ["@99protsenti", "prettier"],
}
